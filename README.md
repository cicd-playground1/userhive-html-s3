# userhive-html-s3

_This project is part of a "Coding Challenge"_

It mostly contains the pipeline code needed to run the UserHive Python script and upload the generated user list to a S3 bucket.

## Latest user list
http://userhive-prod.s3-website-us-east-1.amazonaws.com


## How it works

`tf-s3` is a terraform module that uploads a HTML and CSS file to S3 and makes it publicly available

`.gitlab-ci.yml`

* GitLab-managed Terraform state is used to store the state file