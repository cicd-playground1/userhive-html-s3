terraform {
  backend "http" {
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_s3_bucket" "html_bucket" {
  bucket = "userhive-prod"
  
  tags = {
    Name = "UserHive data"
  }
}

resource "aws_s3_bucket_acl" "html_bucket" {
  bucket = aws_s3_bucket.html_bucket.id
  acl    = "public-read"
}

resource "aws_s3_object" "html_file" {
  bucket = aws_s3_bucket.html_bucket.id
  key    = "users.html"
  source = var.USERHIVE_OUTPUT_HTML
  etag   = "${filemd5("${var.USERHIVE_OUTPUT_HTML}")}"
  acl    = "public-read"
  content_type = "text/html"
}

resource "aws_s3_object" "css_file" {
  bucket = aws_s3_bucket.html_bucket.id
  key    = "list.css"
  source = var.USERHIVE_OUTPUT_CSS
  etag   = "${filemd5("${var.USERHIVE_OUTPUT_CSS}")}"
  acl    = "public-read"
  content_type = "text/css"
}

output "html_file_url" {
  value = aws_s3_object.html_file.id
}
