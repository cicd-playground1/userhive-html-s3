# Need to be set via env with TF_VAR prefix
variable "USERHIVE_OUTPUT_HTML" {
  description = "Path in the GitLab pipeline artifacts to HTML file"
  type        = string
}

variable "USERHIVE_OUTPUT_CSS" {
  description = "Path in the GitLab pipeline artifacts to CSS file"
  type        = string
}